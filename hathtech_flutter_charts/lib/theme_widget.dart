import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'theme.dart';

/// Applies a theme to descendant Syncfusion widgets.
class HTTheme extends StatelessWidget {
  const HTTheme({
    Key key,
    this.data,
    @required this.child,
  }) : super(key: key);

  /// Specifies a widget that can hold single child.
  final Widget child;

  /// Specifies the color and typography values for descendant widgets.
  final HTThemeData data;

  //ignore: unused_field
  static final HTThemeData _kFallbackTheme = HTThemeData.fallback();

  /// The data from the closest [HTTheme] instance that encloses the given
  /// context.
  ///
  /// Defaults to [HTThemeData.fallback] if there is no [HTTheme] in the given
  /// build context.
  static HTThemeData of(BuildContext context) {
    final _HTInheritedTheme inheritedTheme =
        context.dependOnInheritedWidgetOfExactType<_HTInheritedTheme>();
    return inheritedTheme?.data ??
        (Theme.of(context).brightness == Brightness.light
            ? HTThemeData.light()
            : HTThemeData.dark());
  }

  @override
  Widget build(BuildContext context) {
    return _HTInheritedTheme(data: data, child: child);
  }
}

class _HTInheritedTheme extends InheritedTheme {
  const _HTInheritedTheme({Key key, this.data, Widget child})
      : super(key: key, child: child);

  final HTThemeData data;

  @override
  bool updateShouldNotify(_HTInheritedTheme oldWidget) =>
      data != oldWidget.data;

  @override
  Widget wrap(BuildContext context, Widget child) {
    final _HTInheritedTheme ancestorTheme =
        context.findAncestorWidgetOfExactType<_HTInheritedTheme>();
    return identical(this, ancestorTheme)
        ? child
        : HTTheme(data: data, child: child);
  }
}

/// Holds the color and typography values for light and dark themes. Use
///  this class to configure a [HTTheme] widget.
///
/// To obtain the current theme, use [HTTheme.of].
class HTThemeData with Diagnosticable {
  factory HTThemeData({
    Brightness brightness,
    HTChartThemeData chartThemeData,
  }) {
    brightness ??= Brightness.light;
    chartThemeData = chartThemeData ?? HTChartThemeData(brightness: brightness);

    return HTThemeData.raw(
      brightness: brightness,
      chartThemeData: chartThemeData,
    );
  }

  /// Create a [HTThemeData] given a set of exact values. All the values must be
  /// specified.
  ///
  /// This will rarely be used directly. It is used by [lerp] to
  /// create intermediate themes based on two themes created with the
  /// [HTThemeData] constructor.
  const HTThemeData.raw({
    @required this.brightness,
    @required this.chartThemeData,
  })  : assert(brightness != null),
        assert(chartThemeData != null);

  factory HTThemeData.light() => HTThemeData(brightness: Brightness.light);

  factory HTThemeData.dark() => HTThemeData(brightness: Brightness.dark);

  /// The default color theme. Same as [HTThemeData.light].
  ///
  /// This is used by [HTTheme.of] when no theme has been specified.
  factory HTThemeData.fallback() => HTThemeData.light();

  /// The brightness of the overall theme of the application for the Syncusion widgets.
  final Brightness brightness;

  /// Defines the default configuration of chart widgets.
  final HTChartThemeData chartThemeData;

  /// Creates a copy of this theme but with the given fields replaced with the new values.
  HTThemeData copyWith({
    Brightness brightness,
    HTChartThemeData chartThemeData,
  }) {
    return HTThemeData.raw(
      brightness: brightness ?? this.brightness,
      chartThemeData: chartThemeData ?? this.chartThemeData,
    );
  }

  static HTThemeData lerp(HTThemeData a, HTThemeData b, double t) {
    assert(a != null);
    assert(b != null);
    assert(t != null);
    return HTThemeData.raw(
      brightness: t < 0.5 ? a.brightness : b.brightness,
      chartThemeData:
          HTChartThemeData.lerp(a.chartThemeData, b.chartThemeData, t),
    );
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    final HTThemeData otherData = other;
    return otherData.brightness == brightness &&
        otherData.chartThemeData == chartThemeData;
  }

  @override
  int get hashCode {
    final List<Object> values = <Object>[
      brightness,
      chartThemeData,
    ];
    return hashList(values);
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    final HTThemeData defaultData = HTThemeData.fallback();
    properties.add(EnumProperty<Brightness>('brightness', brightness,
        defaultValue: defaultData.brightness));
    properties.add(DiagnosticsProperty<HTChartThemeData>(
        'chartThemeData', chartThemeData,
        defaultValue: defaultData.chartThemeData));
  }
}
