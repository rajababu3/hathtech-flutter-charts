## Getting started

Import the following package.

```dart
import 'package:hathtech_flutter_charts/charts.dart';
```

### Add chart to the widget tree

Add the chart widget as a child of any widget. Here, the chart widget is added as a child of container widget.

```dart
@override
Widget build(BuildContext context) {
  return Scaffold(
    body: Center(
        child: Container(
          child: HTCartesianChart(
          )
        )
      )
  );
}
```

### Bind data source

Based on data, initialize the appropriate axis type and series type. In the series, map the data source and the fields for x and y data points. To render a line chart with category axis, initialize appropriate properties.

```dart
@override
Widget build(BuildContext context) {
  return Scaffold(
    body: Center(
        child: Container(
          child: HTCartesianChart(
            // Initialize category axis
            primaryXAxis: CategoryAxis(),

            series: <LineSeries<SalesData, String>>[
              LineSeries<SalesData, String>(
                // Bind data source
                dataSource:  <SalesData>[
                  SalesData('Jan', 35),
                  SalesData('Feb', 28),
                  SalesData('Mar', 34),
                  SalesData('Apr', 32),
                  SalesData('May', 40)
                ],
                xValueMapper: (SalesData sales, _) => sales.year,
                yValueMapper: (SalesData sales, _) => sales.sales
              )
            ]
          )
        )
      )
  );
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}

```

**Note**

- Use `HTCartesianChart` widget to render line, spline, area, column, bar, bubble, scatter, step line, and fast line charts.
- Use `HTCircularChart` widget to render pie, doughnut, and radial bar charts.
- Use `HTPyramidChart` and `HTFunnelChart` to render pyramid and funnel charts respectively.

### Add chart elements

Add the chart elements such as title, legend, data label, and tooltip to display additional information about the data plotted in the chart.

```dart
@override
Widget build(BuildContext context) {
  return Scaffold(
    body: Center(
        child: Container(
          child: HTCartesianChart(

            primaryXAxis: CategoryAxis(),
            // Chart title
            title: ChartTitle(text: 'Half yearly sales analysis'),
            // Enable legend
            legend: Legend(isVisible: true),
            // Enable tooltip
            tooltipBehavior: TooltipBehavior(enable: true),

            series: <LineSeries<SalesData, String>>[
              LineSeries<SalesData, String>(
                dataSource:  <SalesData>[
                  SalesData('Jan', 35),
                  SalesData('Feb', 28),
                  SalesData('Mar', 34),
                  SalesData('Apr', 32),
                  SalesData('May', 40)
                ],
                xValueMapper: (SalesData sales, _) => sales.year,
                yValueMapper: (SalesData sales, _) => sales.sales,
                // Enable data label
                dataLabelSettings: DataLabelSettings(isVisible: true)
              )
            ]
          )
        )
      )
  );
}
```
